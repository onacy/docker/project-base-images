#!/usr/bin/env bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

docker login registry.gitlab.com

tag="registry.gitlab.com/onacy/docker/project-base-images/syncer"
docker build "$DIR" -t "$tag"
docker push "$tag"
