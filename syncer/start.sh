#!/usr/bin/env bash

set -e

cp /home/app/app.prf /home/app/.unison/app.prf

echo "Start unison"

until unison app; do
    echo "Unison crashed with code $?.  Respawning.." >&2
    sleep 1
done