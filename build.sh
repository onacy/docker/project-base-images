#!/usr/bin/env bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

$DIR/php/build.sh
$DIR/php/apache-php/build.sh
$DIR/syncer/build.sh

printf "\n\n✓ Everything done \n\n"