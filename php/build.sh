#!/usr/bin/env bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

docker login registry.gitlab.com

declare -a php_versions=("7.0" "7.1" "7.2" "7.3" "7.4" "8.0" "8.1")

for version in "${php_versions[@]}" # Later add further versions here
do
    printf "\n\n> Building PHP-Version $version \n\n"
    tag="registry.gitlab.com/onacy/docker/project-base-images/php-fpm-$version"
    docker build "$DIR" -t "$tag" --build-arg "PHP_VERSION=$version"
    
    printf "\n\n> Pushing PHP-Version $version \n\n"
    docker push "$tag"
done